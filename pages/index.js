
// MY FIRST EXEMPLE FROM NEXT JS & TAILWIND

import fetch from "node-fetch";
import Link from "next/link";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import styles from "../styles/Home.module.css"
import Head from "next/head";

function Blog({ posts }) {
  return (
    <>
    <Head>
      <title>Oeuvre Liste | Home</title>
      <meta name="description" content="oeuvres"></meta>
    </Head>
    <div>
      
      <h1>home</h1>
      <Link href="/oeuvres">
        <a>See All oeuvres</a>
      </Link>
    </div>
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch("https://beta.api.danae.io/test");
  const data = await res.json();
  const posts = await data;

  return {
    props: {
      posts,
    },
  };
}

export default Blog;

// import styles from '../styles/Oeuvres.module.css'
// import Link from "next/link"

// export const getStaticProps = async () => {
    
//     const res = await fetch('https://beta.api.danae.io/test');
//     const data = await res.json();
//     return {
//         props: { oeuvresList: data}
//     }
// }
// const Oeuvres = ({ oeuvresList }) => {
    
//     return (
//         <div>
//             <h1>All oeuvres</h1>
//             {oeuvresList.map(oeuvre => ( 
//                 <Link href={'/oeuvres/' + oeuvre.id} key={oeuvre.id }>
//                     <a className={styles.oeuvre}>
//                         <h3>{ oeuvre.title }</h3>
//                     </a>
//                 </Link>
//             ))}
            
//         </div>
//     );
// };



// export default Oeuvres;

// import styles from '../styles/Oeuvres.module.css'
// import Link from "next/link"
// import { Zoom } from "react-slideshow-image";
// import "react-slideshow-image/dist/styles.css";
// import Slideshow from '../components/SlideShow';



// export const getStaticProps = async () => {
    
//   const res = await fetch('https://beta.api.danae.io/test/39');
//   const data = await res.json();

//   return {
//       props: { oeuvre: data}
//   }
// }


// const zoomInProperties = {
//   indicators: true,
//   scale: 1.2,
//   infinite: true,
//   arrows: false,
//   autoplay: false
 
// };


// const oeuvres = ({ oeuvre }) => {
    
//   return (
//       <div>
//           <h1>All oeuvres</h1>
         
//           <Zoom {...zoomInProperties}>
			
// 					<div  className="flex justify-center w-full h-full">
// 						<img
// 							className="object-cover w-3/4 rounded-lg shadow-xl"
// 							src={ oeuvre.pictures[0] }
// 						/>
// 					</div>
			
// 			    </Zoom>
             
//             <h3>{ oeuvre.title }</h3>
//             <p>{ oeuvre.price }</p>
//             <p>{ oeuvre.artist.last_name }</p>
//             <p>{ oeuvre.artist.first_name }</p>
//             <p>{ oeuvre.artist.username }</p>
            
         
//             <Link href="/oeuvres">
//         <a>Liste d oeuvre</a>
//       </Link>
//       </div>
      
//   );
// };

// export default oeuvres;
