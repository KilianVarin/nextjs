import Image from 'next/image';
import Slideshow from '../../components/SlideShow';
import { Zoom } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import styles from '../../styles/Oeuvres.module.css'
import Link from "next/link"
import Oeuvres from '../index';
import  Counter  from '../../components/counter';
import Countdown from 'react-countdown';
import { Button } from '@mui/material';

const Completionist = () => <Link href={'/home/'}>
    <Button variant="outlined">Buy</Button></Link>;

// Renderer callback with condition
const renderer = ({ hours, minutes, seconds, completed }) => {
  if (completed) {
    // Render a completed state
    return <Completionist />;
  } else {
    // Render a countdown
    return <span>{hours}:{minutes}:{seconds}</span>;
  }
};

export const getStaticPaths = async () => {
    const res = await fetch('https://beta.api.danae.io/test');
    const data = await res.json();

    const paths = data.map(oeuvre => {
        return {
            params: { id: oeuvre.id.toString() }
        }
    })
    return {
        paths,
        fallback: false
    }
}


export const getStaticProps = async (context) => {
    const id = context.params.id;
    const res = await fetch('https://beta.api.danae.io/test/' + id);
    const price = await fetch('https://min-api.cryptocompare.com/data/price?fsym=MATIC&tsyms=EUR')
    const data = await res.json();
    const datas = await price.json();


    return {
        props: { oeuvre: data, price: datas}
    }
}   

const zoomInProperties = {
    indicators: true,
    scale: 1.2,
    duration: 5000,
    transitionDuration: 500,
    infinite: true,
    prevArrow: (
        <div style={{ width: "30px", marginRight: "-30px", cursor: "pointer" }}>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"
                fill="#2e2e2e"
            >
                <path d="M242 180.6v-138L0 256l242 213.4V331.2h270V180.6z" />
            </svg>
        </div>
    ),
    nextArrow: (
        <div style={{ width: "30px", marginLeft: "-30px", cursor: "pointer" }}>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"
                fill="#2e2e2e"
            >
                <path d="M512 256L270 42.6v138.2H0v150.6h270v138z" />
            </svg>
        </div>
    ),
};


const oeuvres = ({ oeuvre, price }) => {
    
    const images = [
		oeuvre.pictures[0],
		oeuvre.pictures[1]
	
	];
    return (
        <div>
            <Zoom {...zoomInProperties}>
				{images.map((each, index) => (
					<div key={index} className="flex justify-center w-full h-full">
						<img
							className="object-cover w-3/4 rounded-lg shadow-xl"
							src={each}
						/>
					</div>
				))}
			</Zoom>
            <h1>{ oeuvre.title }</h1>
            <p>{ oeuvre.price }<img src="/Vector.png" /></p>
            <h2>{(2 - price.EUR) * (Math.round(oeuvre.price ))}€</h2>
            {/* <img src={ oeuvre.pictures[0] } />
            <img src={ oeuvre.pictures[1] } />  */}
            <p>{ oeuvre.artist.last_name }</p>
            <p>{ oeuvre.artist.first_name }</p>
            {/* <p>{ oeuvre.artist.username }</p> */}
            <h2>Sale Ends In</h2><Countdown
    date={Date.now() + 15000000}
    
  />
  <Link href={'/sold/'  + oeuvre.id}>
    <Button variant="outlined">Buy</Button></Link>
           {/* <Slideshow /> */}
           {/* <h1>All oeuvres</h1>
            
                <Link href={'/oeuvres/' + oeuvre.id} key={oeuvre.id }>
                    <a className={styles.oeuvre}>
                        <h3>{ oeuvre.title }</h3>
                    </a>
                </Link> */}
           
        </div>
    );
};








export default oeuvres;