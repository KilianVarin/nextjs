import styles from '../../styles/Oeuvres.module.css'
import Link from "next/link"
import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, Grid, ListItem } from '@mui/material';
import { join } from 'path';
import { Zoom } from "react-slideshow-image";
import Counter from '../../components/counter';


export const getStaticProps = async () => {
    
    const res = await fetch('https://beta.api.danae.io/test');
    const result = await fetch('https://beta.api.danae.io/test/39');
    const datas = await res.json();
    const data = await result.json();
    return {
        props: { joins: datas, observatory: data }
    }
}

const zoomInProperties = {
    indicators: false,
    scale: 1.2,
    duration: 5000,
    transitionDuration: 500,
    infinite: true,
    autoplay: false,
    arrows: false,

    prevArrow: (
        <div style={{ width: "30px", marginRight: "-30px", cursor: "pointer" }}>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"
                fill="#2e2e2e"
            >
                <path d="M242 180.6v-138L0 256l242 213.4V331.2h270V180.6z" />
            </svg>
        </div>
    ),
    nextArrow: (
        <div style={{ width: "30px", marginLeft: "-30px", cursor: "pointer" }}>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"
                fill="#2e2e2e"
            >
                <path d="M512 256L270 42.6v138.2H0v150.6h270v138z" />
            </svg>
        </div>
    ),
};


const joins = ({ joins, observatory }) => {
    return (
        <> 
            <div class="flex flex-row">
            <div class="basis-1/4"></div>
                <div class="basis-1/2">
                        <Zoom {...zoomInProperties} >
                        <img src={observatory.pictures} />
                        </Zoom>
                </div>    
                <div class="basis-1/4">
                        <h1>{observatory.title}</h1>
                        <p>{ observatory.artist.last_name }</p>
                        <Link href={'/home/' + observatory.id} key={observatory.id }>
                        <Button variant="outlined">About</Button></Link>
                        <Counter />
                </div>    
            </div>       


        <div className="flex flex-row">
           {joins.map(join => ( 
            <div class="basis-1/4 check">
                <Link href={'/home/' + join.id} key={join.id }>
                    <Card sx={{ maxWidth: 345 }}  >
                        <CardActionArea>
                            <CardMedia
                            component="img"
                            height="20"
                            image={join.pictures[0]}
                            alt="green iguana"
                            />
                            <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                            { join.title }
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                            { join.artist.last_name }
                            </Typography>
                            </CardContent>
                        </CardActionArea>
                        </Card>
                    {/* <a className={styles.oeuvre}>
                        <h3>{ oeuvre.title }</h3>
                        <p>{ oeuvre.artist.last_name }</p>
                        <img src={oeuvre.pictures[0]} />
                    </a> */}
                </Link>
                </div>
            ))}           
        </div>
        </>
    );
};



export default joins;