import styles from '../../styles/Oeuvres.module.css'
import Link from "next/link"
import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, Grid, ListItem } from '@mui/material';
import { join } from 'path';
import { Zoom } from "react-slideshow-image";
import Countdown from 'react-countdown';

export const getStaticPaths = async () => {
    const res = await fetch('https://beta.api.danae.io/test');
    const data = await res.json();

    const paths = data.map(oeuvre => {
        return {
            params: { id: oeuvre.id.toString() }
        }
    })
    return {
        paths,
        fallback: false
    }
}

export const getStaticProps = async (context) => {
    const id = context.params.id;
    const res = await fetch('https://beta.api.danae.io/test/' + id);

    const data = await res.json();



    return {
        props: { oeuvre: data}
    }
} 



const Completionist = () => <Link href={'/buying/' + join.id} key={join.id }>
    <Button variant="outlined">Click Here !</Button>
   </Link>;

// Renderer callback with condition
const renderer = ({ hours, minutes, seconds, completed }) => {
  if (completed) {
    // Render a completed state
    return <Completionist />;
  } else {
    // Render a countdown
    return <span>{hours}:{minutes}:{seconds}</span>;
  }
};


const joins = ({ joins, observatory }) => {
    return (
        <>
          
            <h1>Redirecting ...</h1>
            <h1>{observatory.title}</h1>
            <p>{ observatory.artist.last_name }</p>
            
            <Countdown
    date={Date.now() + 5000}
    renderer={renderer}
  />,
        </>
    );
};



export default joins;