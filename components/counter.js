import { useEffect, useState } from 'react';
import { calculateTimeLeft } from '../counter'



const Counter = () => {
  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  useEffect(() => {
    setTimeout(() => setTimeLeft(calculateTimeLeft()), 1000);
  }, [timeLeft]);

  return (
    <div className="counter">
      <div className="counter-item">
        <span className="value">{String(timeLeft.days).padStart(0, '0')}</span>
        <span className="label">Day</span>
      </div>

      <div className="counter-item">
        <span className="value">{String(timeLeft.hours).padStart(0, '0')}</span>
        <span className="label">Hours</span>
      </div>

      <div className="counter-item">
        <span className="value">
          {String(timeLeft.minutes).padStart(2, '0')}
        </span>
        <span className="label">Minutes</span>
      </div>
      <div className="counter-item">
        <span className="value">
          {String(timeLeft.seconds).padStart(2, '0')}
        </span>
        <span className="label">Segundos</span>
      </div>
    </div>
  );
};

export default Counter;