/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images : {
    domains : ["https://beta.api.danae.io/test"],
  },
}

module.exports = nextConfig
